package libwebtopaygo

import (
	"fmt"
	"net/url"
)

type MacroRequest struct {
	ProjectID        uint   `json:"projectid,omitempty"`
	OrderID          string `json:"orderid,omitempty"`
	AcceptURL        string `json:"accepturl,omitempty"`
	CancelURL        string `json:"cancelurl,omitempty"`
	CallbackURL      string `json:"callbackurl,omitempty"`
	Version          string `json:"version,omitempty"`
	Lang             string `json:"lang,omitempty"`
	Amount           int    `json:"amount,omitempty"`
	Currency         string `json:"currency,omitempty"`
	Payment          string `json:"payment,omitempty"`
	Country          string `json:"country,omitempty"`
	Paytext          string `json:"paytext,omitempty"`
	PFirstName       string `json:"p_fistname,omitempty"`
	PLastName        string `json:"p_lastname,omitempty"`
	PEmail           string `json:"p_email,omitempty"`
	PStreet          string `json:"p_street,omitempty"`
	PCity            string `json:"p_city,omitempty"`
	PState           string `json:"p_state,omitempty"`
	PZip             string `json:"p_zip,omitempty"`
	PCountryCode     string `json:"p_countrycode,omitempty"`
	OnlyPayments     string `json:"only_payments,omitempty"`
	DisallowPayments string `json:"disalow_payments,omitempty"`
	Test             bool   `json:"test,omitempty"`
	TimeLimit        string `json:"time_limit,omitempty"`
	PersonCode       string `json:"personcode,omitempty"`
	DeveloperID      string `json:"developerid,omitempty"`
}

func (m MacroRequest) ToBase64String() string {
	dataQueryParams := url.Values{}
	dataQueryParams.Set("projectid", fmt.Sprint(m.ProjectID))
	dataQueryParams.Set("orderid", m.OrderID)
	dataQueryParams.Set("accepturl", m.AcceptURL)
	dataQueryParams.Set("cancelurl", m.CancelURL)
	dataQueryParams.Set("callbackurl", m.CallbackURL)
	dataQueryParams.Set("version", m.Version)
	addQueryParam(&dataQueryParams, "lang", m.Lang)
	addQueryParam(&dataQueryParams, "amount", fmt.Sprint(m.Amount))
	addQueryParam(&dataQueryParams, "currency", m.Currency)
	addQueryParam(&dataQueryParams, "payment", m.Payment)
	addQueryParam(&dataQueryParams, "country", m.Country)
	addQueryParam(&dataQueryParams, "paytext", m.Paytext)
	addQueryParam(&dataQueryParams, "p_firstname", m.PFirstName)
	addQueryParam(&dataQueryParams, "p_lastname", m.PLastName)
	addQueryParam(&dataQueryParams, "p_email", m.PEmail)
	addQueryParam(&dataQueryParams, "p_street", m.PStreet)
	addQueryParam(&dataQueryParams, "p_city", m.PCity)
	addQueryParam(&dataQueryParams, "p_zip", m.PZip)
	addQueryParam(&dataQueryParams, "p_countrycode", m.PCountryCode)
	addQueryParam(&dataQueryParams, "only_payments", m.OnlyPayments)
	addQueryParam(&dataQueryParams, "disalow_payments", m.DisallowPayments)
	if m.Test {
		dataQueryParams.Set("test", "1")
	}
	addQueryParam(&dataQueryParams, "time_limit", m.TimeLimit)
	addQueryParam(&dataQueryParams, "personcode", m.PersonCode)
	addQueryParam(&dataQueryParams, "developerid", m.DeveloperID)

	dataQuery := HttpBuildQuery(dataQueryParams)
	dataQueryAsBase64 := EncodeBase64(dataQuery)
	return dataQueryAsBase64
}

func addQueryParam(params *url.Values, key, val string) {
	if val != "" {
		params.Set(key, val)
	}
}
