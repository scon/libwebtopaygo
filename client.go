package libwebtopaygo

import (
	"errors"
	"net/url"
)

const (
	VERSION = "1.6"
	PAY_URL = "https://www.paysera.com/pay/"
)

// Client of Paysera project.
type Client struct {
	ProjectID    uint
	SignPassword string
}

// NewClient returns new paysera client
func NewClient(projectID uint, signPassword string) (*Client, error) {
	if projectID == 0 || signPassword == "" {
		return nil, errors.New("sign password and project ID is required")
	}
	return &Client{projectID, signPassword}, nil
}

func (c Client) NewMacroRequest() MacroRequest {
	return MacroRequest{ProjectID: c.ProjectID, Version: VERSION}
}

func BuildRequestURL(request MacroRequest, signPassword string) string {
	data := request.ToBase64String()
	sign := GetMD5Hash(data + signPassword)
	payload := url.Values{}
	payload.Add("data", data)
	payload.Add("sign", sign)
	return PAY_URL + "?" + payload.Encode()
}

func (c Client) GetMacroCallbackData(params url.Values) (MacroCallback, error) {
	macroCallback := MacroCallback{}
	ss1, ok := params["ss1"]
	if !ok {
		return macroCallback, errors.New("missed parameter: ss1 parameter is empty")
	}

	dataAsBase64, ok := params["data"]
	if !ok {
		return macroCallback, errors.New("missed parameter: data parameter is empty")
	}

	if GetMD5Hash(dataAsBase64[0]+c.SignPassword) != ss1[0] {
		return macroCallback, errors.New("ss1 parameter is not valid")
	}

	decodedData, err := DecodeBase64(dataAsBase64[0])
	if err != nil {
		return macroCallback, err
	}
	dataValues, err := url.ParseQuery(decodedData)
	if err != nil {
		return macroCallback, err
	}
	macroCallback.MacroCallbackData(dataValues)
	return macroCallback, nil
}
