package libwebtopaygo

import (
	"net/url"
	"testing"
)

type HttpBuildQueryTest struct {
	queryParams url.Values
	expected    string
}

var httpBuildQueryTest = []HttpBuildQueryTest{
	{url.Values{"data": []string{"testdata"}}, "data=testdata"},
	{url.Values{"sign": []string{"123456789"}}, "sign=123456789"},
	{url.Values{
		"data": []string{"testdata"},
		"sign": []string{"123456789"},
	}, "data=testdata&sign=123456789"},
}

func TestHttpBuildQuery(t *testing.T) {
	for _, test := range httpBuildQueryTest {
		result := HttpBuildQuery(test.queryParams)
		if result != test.expected {
			t.Fatal("TestHttpBuildQuery expected: " + test.expected + " Get: " + result)
		}
	}
}

type QueryBase64Test struct {
	dataQuery         string
	dataQueryAsBase64 string
}

var queryBase64Test = []QueryBase64Test{
	{"data=testdata", "ZGF0YT10ZXN0ZGF0YQ=="},
	{"sign=123456789", "c2lnbj0xMjM0NTY3ODk="},
	{"data=testdata&sign=123456789", "ZGF0YT10ZXN0ZGF0YSZzaWduPTEyMzQ1Njc4OQ=="},
}

func TestEncodeBase64(t *testing.T) {
	for _, test := range queryBase64Test {
		result := EncodeBase64(test.dataQuery)
		if result != test.dataQueryAsBase64 {
			t.Fatal("TestEncodeBase64 expected: " + test.dataQueryAsBase64 + " Get: " + result)
		}
	}
}

func TestDecodeBase64(t *testing.T) {
	for _, test := range queryBase64Test {
		expDataQuery, err := DecodeBase64(test.dataQueryAsBase64)
		if err != nil {
			t.Fatal(err)
		}
		if expDataQuery != test.dataQuery {
			t.Fatal("TestDecodeBase64 expected: " + test.dataQuery + " Get: " + expDataQuery)
		}
	}
}

type GetMD5HashTest struct {
	data    string
	MD5Hash string
}

var getMD5HashTest = []GetMD5HashTest{
	{"testdata", "ef654c40ab4f1747fc699915d4f70902"},
	{"123456789", "25f9e794323b453885f5181f1b624d0b"},
	{"testdata123456789", "1808f4e5d6f72b4b96891d25c70805c6"},
}

func TestGetMD5Hash(t *testing.T) {
	for _, test := range getMD5HashTest {
		expMD5Hash := GetMD5Hash(test.data)
		if expMD5Hash != test.MD5Hash {
			t.Fatal("TestGetMD5Hash expected: " + test.MD5Hash + " Get: " + expMD5Hash)
		}
	}
}
