module bitbucket.org/scon/libwebtopaygo

go 1.13

require (
	github.com/go-chi/chi v4.0.2+incompatible
	golang.org/x/net v0.0.0-20191002035440-2ec189313ef0 // indirect
)
