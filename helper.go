package libwebtopaygo

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"log"
	"net/url"
)

func HttpBuildQuery(queryData url.Values) string {
	return queryData.Encode()
}

func EncodeBase64(query string) string {
	return base64.URLEncoding.EncodeToString([]byte(query))
}

func DecodeBase64(data string) (string, error) {
	decodedData, err := base64.URLEncoding.DecodeString(data)
	return string(decodedData), err
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func CheckErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
