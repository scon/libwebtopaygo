package main

import (
	"libwebtopaygo"
	"log"
)

func main() {
	client, err := libwebtopaygo.NewClient(140405, "7f446b1dce029855d89f05ff444080be")
	if err != nil {
		log.Fatalln(err)
	}
	request := client.NewMacroRequest()
	request.OrderID = "19"
	request.AcceptURL = "http://188.166.81.121:8080/accept"
	request.CancelURL = "http://188.166.81.121:8080/cancel"
	request.CallbackURL = "http://188.166.81.121:8080/callback"
	request.Test = true

	redirectURL := libwebtopaygo.BuildRequestURL(request, client.SignPassword)
	log.Println(redirectURL)
}
