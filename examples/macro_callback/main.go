package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"libwebtopaygo"
	"log"
	"net/http"
)

var client = libwebtopaygo.NewClient(140405, "7f446b1dce029855d89f05ff444080be")

func main() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/callback", PayseraCallBack)
	http.ListenAndServe(":8080", r)
}

func PayseraCallBack(w http.ResponseWriter, r *http.Request) {
	macroCallback, err := client.GetMacroCallbackData(r.URL.Query())
	if err != nil {
		log.Fatalln(err)
		return
	}
	log.Println(macroCallback)
	if macroCallback.Test {
		log.Println("Testing, real payment was not made")
		// return
	}
	if macroCallback.Status != 1 {
		log.Println("Payment processed success")
		// return
	}
	w.Write([]byte(libwebtopaygo.MacroCallbackResponseStatusOK))
}
