package libwebtopaygo

import (
	"net/url"
	"strconv"
)

const (
	MacroCallbackResponseStatusOK = "OK"
)

// MacroCallback represents macro payment callback structure
type MacroCallback struct {
	ProjectID        uint
	OrderID          string
	Lang             string
	Amount           int
	Currency         string
	Payment          string
	Country          string
	Paytext          string
	Name             string
	Surename         string
	Status           uint
	Test             bool
	PaymentCountry   string
	PayerIPCountry   string
	PayerCountry     string
	PEmail           string
	RequestID        string
	PayAmount        int
	PayCurrency      string
	Version          string
	Account          string
	PersonCodeStatus uint
}

func (m *MacroCallback) MacroCallbackData(dataQueryParams url.Values) {
	projectID, err := strconv.ParseUint(dataQueryParams.Get("projectid"), 0, 64)
	CheckErr(err)
	m.ProjectID = uint(projectID)
	m.OrderID = dataQueryParams.Get("orderid")
	m.Lang = dataQueryParams.Get("lang")
	amount, err := strconv.ParseInt(dataQueryParams.Get("amount"), 0, 64)
	CheckErr(err)
	m.Amount = int(amount)
	m.Currency = dataQueryParams.Get("currency")
	m.Payment = dataQueryParams.Get("payment")
	m.Country = dataQueryParams.Get("country")
	m.Paytext = dataQueryParams.Get("paytext")
	m.Name = dataQueryParams.Get("name")
	m.Surename = dataQueryParams.Get("surename")
	status, err := strconv.ParseUint(dataQueryParams.Get("status"), 0, 64)
	CheckErr(err)
	m.Status = uint(status)
	if dataQueryParams.Get("test") != "0" {
		m.Test = true
	} else {
		m.Test = false
	}
	m.PaymentCountry = dataQueryParams.Get("payment_country")
	m.PayerIPCountry = dataQueryParams.Get("payer_ip_country")
	m.PayerCountry = dataQueryParams.Get("payer_country")
	m.PEmail = dataQueryParams.Get("p_email")
	m.RequestID = dataQueryParams.Get("requestid")
	payamount, err := strconv.ParseInt(dataQueryParams.Get("payamount"), 0, 64)
	CheckErr(err)
	m.PayAmount = int(payamount)
	m.PayCurrency = dataQueryParams.Get("paycurrency")
	m.Version = dataQueryParams.Get("version")
	m.Account = dataQueryParams.Get("account")
	if dataQueryParams.Get("personcodestatus") != "" {
		personcodestatus, err := strconv.ParseUint(dataQueryParams.Get("personcodestatus"), 0, 64)
		CheckErr(err)
		m.PersonCodeStatus = uint(personcodestatus)
	}
}
